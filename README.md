## planta-app
### pt-br

Este é um projeto de software destino acadêmico relacionado ao trabalho de conclusão de curso (**TCC**) de Sistemas de informação.
O tema do projeto aborda sustentabilidade, automação e aplicação web/app.
Automação de rega para manter a saúde de temperos e hortaliças.

Nele contém lógicas elaboradas para criar reatividade na interface e uma excelente UI e UX.
Contém:
- interface de validação por input.
- interface de configuração de *WiFi*.
- interface administrativa com gráficos e dados objetivos além da configuração da planta e irrigação.
- interface de histórico(log).
- interface de imagem.
- interface de perfil.

### eng.

This is an academic destination software project related to the conclusion work of Information Systems.
The project's theme addresses sustainability, automation and web / app application.
Watering automation to maintain the health of spices and vegetables.

It contains logic designed to create reactivity in the interface and an excellent UI and UX.
Contains:
- input validation interface.
- * WiFi * configuration interface.
- administrative interface with graphs and objective data in addition to the configuration of the plant and irrigation.
- history interface (log).
- image interface.
- profile interface.
## Project setup

```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
