import Vue from 'vue'
import VueRouter from 'vue-router'
import License from '../views/License.vue'
import Dashboard from '../views/Dashboard.vue'
import Wifi from '../views/Wifi.vue'
import Images from '../views/Images.vue'
import Records from '../views/Records.vue'
import Configuration from '../views/Configuration.vue'
import PlantConfig from '../views/PlantConfig.vue'
import SelectVase from '../views/SelectVase.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'license',
    component: License
  },
  {
    path: '/addVase',
    name: 'addVase',
    component: License
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard
  },
  {
    path: '/wifi',
    name: 'wifi',
    component: Wifi
  },
  {
    path: '/images',
    name: 'images',
    component: Images
  },
  {
    path: '/records',
    name: 'records',
    component: Records
  },
  {
    path: '/configurations',
    name: 'configs',
    component: Configuration
  },
  {
    path: '/plantConfig',
    name: 'plantConfig',
    component: PlantConfig
  },
  {
    path: '/selectVase',
    name: 'selectVase',
    component: SelectVase
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
