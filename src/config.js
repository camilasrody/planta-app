export const config = (config) => ({
  baseURL: (process.env.NODE_ENV === 'production') ? 'https://planta-server.loca.lt' : 'http://pi-planta.local',
  timeout: 10000,
  ...config,
});