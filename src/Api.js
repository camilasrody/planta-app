import axios from 'axios';
import { config } from './config';

// 'application/x-www-form-urlencoded'
// 'multipart/form-data'

const instance = axios.create(config());
const headerFormData = { 'Content-Type': 'application/x-www-form-urlencoded' };
const BASE_URL_API = (process.env.NODE_ENV === 'production') ? 'https://planta-server.loca.lt' : 'http://pi-planta.local'

instance.defaults.baseURL = BASE_URL_API + '/api';
// instance.defaults.headers.post['Content-Type'] = 'multipart/form-data';
class Api {
 
  static get(url, { header } = {}) {
    return this.fetchForm(url, 'GET', header);
  }
 
  static post(url, { header, payload = {}, multipart } = {}) {
    return this.fetchForm(url, 'POST', header, payload, multipart);
  };
 
  static delete(url, { header, payload = {} } = {}) {
    return this.fetchForm(url, 'DELETE', header, payload);
  };
 
  static put(url, { header, payload = {}, multipart } = {}) {
    return this.fetchForm(url, 'PUT', header, payload, multipart);
  };

  static fetchForm(url, method, header, payload, multipart) {
    let headers = header;

   
    if (multipart) headers = { ...headers, ...headerFormData };

   
    let data = multipart ? this.createFormData(payload) : payload;

   
    if (method === 'GET') data = undefined;

    return new Promise((resolve, reject) => {
      instance({ url, method, headers, data })
        .then((res) => {
          resolve(res.data)
          // if (res.status === 200) resolve(res.data);
          // else if (res.status === 404) reject(null);
          // else reject(res.data);
        }).catch((err) => {
          const defaultErr = { status: 503, msg: 'Service Unavailable' };
          const res = err.response;
          const data = res ? res.data : defaultErr;
          this._onErrorListener(data);
          reject(data);
        });
    });
  }
 
  static createFormData(data) {
    const form = new FormData();
    for (const key in data) {
      form.append(key, data[key]);
    }
    return form;
  }
 
  static _onErrorListener = (() => { });
  static onError(callback) {
    this._onErrorListener = callback;
  }
}

export default Api;