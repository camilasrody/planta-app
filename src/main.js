import Vue from 'vue'
import App from './App.vue'
import router from './router'
import '@/assets/css/tailwind.css'
import Axios from 'axios';


const BASE_URL_API = (process.env.NODE_ENV === 'production') ? 'https://planta-server.loca.lt' : 'http://pi-planta.local'

Axios.defaults.baseURL = BASE_URL_API + '/api';
// Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
